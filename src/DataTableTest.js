import { useEffect, useState } from "react";
import DataTable from "./DataTable";

const fetchAllEmployees = (employees, pageNumber, pageSize) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      const totalPages = employees.length / pageSize;
      const sliceStart = (pageNumber - 1) * pageSize;
      const sliceEnd = sliceStart + pageSize;
      const values = employees.slice(sliceStart, sliceEnd);

      resolve({
        pageNumber,
        pageSize,
        totalElements: employees.length,
        totalPages,
        values,
      });
    }, 2000);
  });
};

const useFetchEmployees = (employees, pageNumber, pageSize) => {
  const [response, setResponse] = useState({
    page: null,
    loading: true,
    error: null,
  });

  const fetchData = async (employees, pageNumber, pageSize) => {
    try {
      setResponse((prev) => ({ ...prev, loading: true, error: null }));
      const page = await fetchAllEmployees(employees, pageNumber, pageSize);
      setResponse({ page, loading: false, error: null });
    } catch (e) {
      setResponse({ page: null, loading: false, error: e?.message });
    }
  };

  useEffect(() => {
    fetchData(employees, pageNumber, pageSize);
  }, [employees, pageNumber, pageSize]);

  return { ...response };
};

const DataTableTest = ({ employees, columns }) => {
  const [{ pageNumber, pageSize }, setPagination] = useState({
    pageNumber: 1,
    pageSize: 20,
  });

  const { page, loading, error } = useFetchEmployees(
    employees,
    pageNumber,
    pageSize
  );

  return (
    <DataTable
      columns={columns}
      rows={page?.values}
      error={error}
      loading={loading}
      pageNumber={pageNumber}
      pageSize={pageSize}
      pageSizeOptions={[5, 10, 20, 40, 60, 80, 100]}
      totalElements={page?.totalElements}
      onPageChange={(pageNumber) =>
        setPagination((prev) => ({ ...prev, pageNumber }))
      }
      onPageSizeChange={(pageSize) =>
        setPagination((prev) => ({ ...prev, pageSize }))
      }
    />
  );
};

export default DataTableTest;
