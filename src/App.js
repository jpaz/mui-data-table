import { Box, Typography } from "@mui/material";
import "./App.css";
import DataTableTest from "./DataTableTest";
import employeesWithSubitems from "./employees-with-subitems.json";
import employees from "./employees.json";

function App() {
  const columns = [
    {
      field: "id",
      headerName: "ID",
      width: 100,
      pinned: "left",
    },
    {
      field: "name",
      headerName: "Name",
      width: 100,
      pinned: "left",
    },
    {
      field: "age",
      headerName: "Age",
      width: 40,
      renderContent: (value) => <Typography color="red">{value}</Typography>,
    },
    {
      field: "gender",
      headerName: "Gender",
      width: 100,
    },
    {
      field: "email",
      headerName: "Email",
      width: 100,
    },
    {
      field: "phone",
      headerName: "Phone",
      width: 100,
    },
    {
      field: "address",
      headerName: "Address",
      width: 100,
      pinned: "right",
    },
    {
      field: "about",
      headerName: "About",
      width: 100,
      pinned: "right",
    },
  ];

  return (
    <Box
      sx={{
        width: "100vw",
        minHeight: "100vh",
      }}
    >
      <Box sx={{ width: "600px", height: "700px" }}>
        <DataTableTest employees={employees} columns={columns} />
      </Box>
      <Box sx={{ width: "600px", height: "700px" }}>
        <DataTableTest employees={employeesWithSubitems} columns={columns} />
      </Box>
    </Box>
  );
}

export default App;
