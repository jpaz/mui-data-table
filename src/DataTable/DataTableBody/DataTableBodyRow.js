import { TableCell, TableRow } from "@mui/material";
import { Box } from "@mui/system";

const DataTableBodyRow = ({ row, columns, isVisible, getSXColumn }) => (
  <>
    <TableRow className="DataTableRow-main">
      {columns
        .filter((column) => isVisible(column.field))
        .map((column, index) => (
          <TableCell
            key={`cell-${index}-${column.field}`}
            sx={getSXColumn(column)}
          >
            <Box className="DataTable-CellContent">
              {column.renderContent
                ? column.renderContent(row[column.field])
                : row[column.field]}
            </Box>
          </TableCell>
        ))}
    </TableRow>

    {row?.subitems?.map((subRow, index) => (
      <TableRow
        key={`data-table-sub-row-${index}`}
        className="DataTableRow-subRow"
      >
        {columns
          .filter((column) => isVisible(column.field))
          .map((column, index) => (
            <TableCell
              key={`cell-${index}-${column.field}`}
              sx={getSXColumn(column)}
            >
              <Box className="DataTable-CellContent">
                {column.renderContent
                  ? column.renderContent(subRow[column.field])
                  : subRow[column.field]}
              </Box>
            </TableCell>
          ))}
      </TableRow>
    ))}
  </>
);

export default DataTableBodyRow;
