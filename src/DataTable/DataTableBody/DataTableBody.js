import {
  CircularProgress,
  TableBody,
  TableCell,
  TableRow,
} from "@mui/material";
import { forwardRef, useContext, useMemo } from "react";
import { DataTableContext } from "../DataTable";
import DataTableBodyRow from "./DataTableBodyRow";

const sortingAscending = (field, rowA, rowB) => {
  const valueA = rowA[field];
  const valueB = rowB[field];

  if (valueA < valueB) {
    return -1;
  }

  if (valueA > valueB) {
    return 1;
  }

  return 0;
};

const sortDescending = (field, rowA, rowB) => {
  const valueA = rowA[field];
  const valueB = rowB[field];

  if (valueA > valueB) {
    return -1;
  }

  if (valueA < valueB) {
    return 1;
  }

  return 0;
};

const DataTableBody = forwardRef(
  ({ rows = [], columns = [], onScroll, loading }, ref) => {
    const { isVisible, sortingOptions, getSXColumn } =
      useContext(DataTableContext);

    const sortedRows = useMemo(() => {
      const sorter = sortingOptions.descending
        ? sortDescending
        : sortingAscending;

      if (typeof sortingOptions.field === "string") {
        return [...rows].sort((rowA, rowB) =>
          sorter(sortingOptions.field, rowA, rowB)
        );
      } else {
        return rows;
      }
    }, [rows, sortingOptions]);

    if (loading) {
      return (
        <TableBody ref={ref} onScroll={onScroll}>
          <TableRow
            sx={{ width: "100%", height: "100%", display: "inline-block" }}
          >
            <TableCell
              sx={{
                width: "100%",
                height: "100%",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <CircularProgress />
            </TableCell>
          </TableRow>
        </TableBody>
      );
    }

    return (
      <TableBody ref={ref} onScroll={onScroll}>
        {sortedRows.map((row, index) => (
          <DataTableBodyRow
            key={`row-${index}`}
            row={row}
            columns={columns}
            isVisible={isVisible}
            getSXColumn={getSXColumn}
          />
        ))}
      </TableBody>
    );
  }
);

export default DataTableBody;
