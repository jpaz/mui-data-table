import { Table, TableContainer } from "@mui/material";
import { anchorRef, usePopupState } from "material-ui-popup-state/hooks";
import { createContext, useMemo, useRef, useState } from "react";
import DataTableBody from "./DataTableBody";
import DataTableFooter from "./DataTableFooter";
import DataTableHead from "./DataTableHead";
import DataTableMenu from "./DataTableMenu";

// TODO: CHANGE TO ENTERPRISE BORDER STYLE
const DEFAULT_BORDER = "1px solid gray";
// border color = cell background color
const DEFAULT_HIDDEN_BORDER = "1px solid white";

const DataTableContext = createContext({
  openTableMenu: () => {},
  showAll: () => {},
  hideAll: () => {},
  setVisibility: (field = "", visibility = true) => {},
  isVisible: (field = "") => !!field,
  setSortField: (field = "", descending = false) => {},
  sortingOptions: {
    field: "",
    descending: false,
  },
  getSXColumn: (column = { field: "" }) => ({
    width: "",
    minWidth: "",
    maxWidth: "",
  }),
});

const DataTable = (props) => {
  const [columnVisibilityModel, setColumnVisibilityModel] = useState({});
  const [sortingOptions, setSortingOptions] = useState({
    field: null,
    descending: false,
  });

  const popupState = usePopupState({
    variant: "popover",
    popupId: `data-table-menu`,
  });

  const tableContainerRef = anchorRef(popupState);

  const context = useMemo(
    () => ({
      // open table menu
      openTableMenu: () => {
        popupState.open(tableContainerRef);
      },
      // show all columns
      showAll: () => {
        setColumnVisibilityModel(
          props.columns.reduce(
            (acc, column) => ({ ...acc, [column.field]: true }),
            {}
          )
        );
      },
      // hide all columns
      hideAll: () => {
        setColumnVisibilityModel(
          props.columns.reduce(
            (acc, column) => ({ ...acc, [column.field]: false }),
            {}
          )
        );
      },
      // set column visibility by field
      setVisibility: (field, visibility) => {
        setColumnVisibilityModel((prev) => ({ ...prev, [field]: visibility }));
      },
      // check column visibility by field
      isVisible: (field) => {
        const visibility = columnVisibilityModel[field];
        const existsInVisibilityModel = typeof visibility === "boolean";
        return !existsInVisibilityModel || visibility;
      },
      // sort rows by field
      setSortField: (field, descending = false) => {
        setSortingOptions((prev) => ({
          ...prev,
          field,
          descending: !!descending,
        }));
      },
      getSXColumn: (column) => {
        const sx = {};

        if (column.width) {
          sx["width"] = column.width;
          sx["minWidth"] = column.width;
          sx["maxWidth"] = column.width;
        }

        if (column.pinned) {
          const index = props.columns.findIndex(
            (col) => col.field === column.field
          );

          const columnsInRange =
            column.pinned === "left"
              ? props.columns.slice(0, index)
              : props.columns.slice(index + 1);

          const position = columnsInRange
            .filter((col) => {
              // check column visibility
              const visibility = columnVisibilityModel[col.field];
              const existsInVisibilityModel = typeof visibility === "boolean";
              // check pinned type (left or right)
              const samePinnedType = col.pinned === column.pinned;
              return (!existsInVisibilityModel || visibility) && samePinnedType;
            })
            .reduce(
              // acc + column width + border width
              (acc, col) => acc + col.width,
              0
            );

          if (column.pinned === "left") {
            sx["left"] = position;
            sx["position"] = "sticky";
            sx["zIndex"] = 100;

            sx[":after"] = {
              content: "''",
              position: "absolute",
              backgroundColor: "gray",
              width: "1px",
              height: "100%",
              bottom: "0px",
              right: "-1px",
            };
          }

          if (column.pinned === "right") {
            sx["right"] = position;
            sx["position"] = "sticky";
            sx["zIndex"] = 100;

            sx[":before"] = {
              content: "''",
              position: "absolute",
              backgroundColor: "gray",
              width: "1px",
              height: "100%",
              bottom: "0px",
              left: "-1px",
            };

            sx["borderRight"] = "none !important";
          }
        }

        return sx;
      },
      sortingOptions,
    }),
    [
      popupState,
      tableContainerRef,
      columnVisibilityModel,
      props.columns,
      sortingOptions,
    ]
  );

  const headRef = useRef();
  const bodyRef = useRef();

  const onHeadScroll = (e) => {
    if (bodyRef.current) {
      bodyRef.current.scrollLeft = e.target.scrollLeft;
    }
  };

  const onBodyScroll = (e) => {
    if (headRef.current) {
      headRef.current.scrollLeft = e.target.scrollLeft;
    }
  };

  return (
    <TableContainer
      ref={tableContainerRef}
      sx={{
        width: "100%",
        height: "calc(100% - 28px)",
        display: "flex",
        flexDirection: "column",
        border: DEFAULT_BORDER,
        borderRadius: "6px",
        padding: "14px",

        "& .MuiTable-root": {
          width: "100%",
          // table height = table-container-height - table-footer-height - padding
          height: "calc(100% - 56px)",
          display: "flex",
          flexDirection: "column",
          borderRadius: "6px",
          borderCollapse: "collapse",
          borderSpacing: 0,
          border: DEFAULT_BORDER,
        },

        "& .MuiTableHead-root": {
          width: "100%",
          height: "56px",
          overflowX: "scroll",
          overflowY: "scroll",
          zIndex: 100,
          position: "sticky",
          top: "0",
          borderBottom: DEFAULT_BORDER,
          borderTopLeftRadius: "6px",
          borderTopRightRadius: "6px",
        },

        "& .MuiTableHead-root::-webkit-scrollbar": {
          visibility: "hidden",
          WebkitAppearance: "none",
          width: "8px",
          height: "8px",
        },

        "& .MuiTableHead-root::-webkit-scrollbar:horizontal": {
          display: "none",
        },

        "& .MuiTableBody-root": {
          width: "100%",
          // table body height = table-height - table-head-height
          height: "calc(100% - 56px)",
          overflowX: "scroll",
          overflowY: "scroll",
        },

        "& .MuiTableRow-root": {
          display: "table",
          width: "100%",
          minWidth: "100%",
          maxWidth: "100%",
        },

        "& .MuiTableBody-root::-webkit-scrollbar": {
          WebkitAppearance: "none",
          width: "8px",
          height: "8px",
        },

        "& .MuiTableBody-root::-webkit-scrollbar-track": {
          backgroundColor: "lightgrey",
        },

        "& .MuiTableBody-root::-webkit-scrollbar-thumb": {
          backgroundColor: "grey",
          borderRadius: "3px",
        },

        "& .MuiTableBody-root::-webkit-scrollbar-corner": {
          backgroundColor: "lightgrey",
          borderBottomRightRadius: "3px",
        },

        "& .MuiTableBody-root::-webkit-scrollbar-track:horizontal": {
          borderBottomLeftRadius: "6px",
        },

        "& .DataTable-Footer": {
          width: "100%",
          height: "56px",
        },

        "& .MuiTableHead-root, .MuiTableCell-root": {
          backgroundColor: "white",
        },

        "& .DataTable-CellContent": {
          width: "100%",
          minWidth: "100%",
          maxWidth: "100%",
          height: "56px",
          minHeight: "56px",
          maxHeight: "56px",
          overflow: "hidden",
          boxSizing: "border-box",
          padding: "6px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
        },

        "& .MuiTableCell-root": {
          padding: "0px",
        },

        "& .MuiTableHead-root .MuiTableCell-root:not(:last-of-type)": {
          borderRight: DEFAULT_BORDER,
        },

        "& .MuiTableHead-root .MuiTableCell-root:last-of-type": {
          borderRight: DEFAULT_HIDDEN_BORDER,
        },

        "& .MuiTableBody-root .MuiTableCell-root": {
          borderBottom: DEFAULT_BORDER,
          borderRight: DEFAULT_BORDER,
        },

        "& .DataTableRow-subRow .MuiTableCell-root": {
          backgroundColor: "lightblue",
        },

        "& .MuiTableBody-root .DataTableRow-main:hover .MuiTableCell-root": {
          backgroundColor: "#F5F5F5",
        },
      }}
    >
      <DataTableContext.Provider value={context}>
        <Table>
          <DataTableHead
            ref={headRef}
            columns={props.columns}
            onScroll={onHeadScroll}
          />

          <DataTableBody
            ref={bodyRef}
            columns={props.columns}
            rows={props.rows}
            onScroll={onBodyScroll}
            loading={props.loading}
          />
        </Table>

        <DataTableFooter
          pageNumber={props.pageNumber}
          pageSize={props.pageSize}
          pageSizeOptions={props.pageSizeOptions}
          totalElements={props.totalElements}
          onPageChange={props.onPageChange}
          onPageSizeChange={props.onPageSizeChange}
        />

        <DataTableMenu columns={props.columns} popupState={popupState} />
      </DataTableContext.Provider>
    </TableContainer>
  );
};

export { DataTableContext };
export default DataTable;
