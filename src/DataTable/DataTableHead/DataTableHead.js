import { TableHead, TableRow } from "@mui/material";
import { forwardRef, useContext } from "react";
import { DataTableContext } from "../DataTable";
import DataTableHeadItem from "./DataTableHeadItem";

const DataTableHead = forwardRef(({ columns = [], onScroll }, ref) => {
  const {
    isVisible,
    setVisibility,
    setSortField,
    sortingOptions,
    openTableMenu,
    getSXColumn,
  } = useContext(DataTableContext);

  return (
    <TableHead ref={ref} onScroll={onScroll}>
      <TableRow>
        {columns
          .filter((column) => isVisible(column.field))
          .map((column) => (
            <DataTableHeadItem
              key={`column-header-${column.field}`}
              column={column}
              openTableMenu={openTableMenu}
              sortingOptions={sortingOptions}
              onChangeColumnVisibility={setVisibility}
              onChangeSortField={setSortField}
              sx={getSXColumn(column)}
            />
          ))}
      </TableRow>
    </TableHead>
  );
});

export default DataTableHead;
