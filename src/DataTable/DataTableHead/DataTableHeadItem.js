import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import {
    Box,
    IconButton,
    Menu,
    MenuItem,
    TableCell,
    Typography
} from "@mui/material";
import { bindMenu, usePopupState } from "material-ui-popup-state/hooks";
import { useRef } from "react";

const DataTableHeaderMenu = ({
  anchorElRef,
  column,
  openTableMenu,
  onChangeColumnVisibility,
  onChangeSortField,
  sortingOptions,
}) => {
  const popupState = usePopupState({
    variant: "popover",
    popupId: `data-table-header-menu-${column.field}`,
  });

  const handleResetOrder = () => {
    onChangeSortField(null);
    popupState.close();
  };

  const handleSortAsc = () => {
    onChangeSortField(column.field, false);
    popupState.close();
  };

  const handleResetNextOrder = () => {
    onChangeSortField(column.field, false, true);
    popupState.close();
  };

  const handleSortDesc = () => {
    onChangeSortField(column.field, true);
    popupState.close();
  };

  const handleHide = () => {
    onChangeColumnVisibility(false);
    popupState.close();
  };

  const handleDisplayHeadMenu = () => {
    openTableMenu();
    popupState.close();
  };

  return (
    <>
      <Box className="DataTable-CellMenu">
        {sortingOptions.field !== column.field && (
          <IconButton onClick={handleSortAsc}>
            <KeyboardArrowUpIcon fontSize="small" />
          </IconButton>
        )}

        {sortingOptions.field === column.field &&
          !sortingOptions.descending && (
            <IconButton onClick={handleSortDesc}>
              <KeyboardArrowUpIcon fontSize="small" />
            </IconButton>
          )}

        {sortingOptions.field === column.field && sortingOptions.descending && (
          <IconButton onClick={handleResetNextOrder}>
            <KeyboardArrowDownIcon fontSize="small" />
          </IconButton>
        )}

        <IconButton onClick={() => popupState.open(anchorElRef.current)}>
          <MoreVertIcon fontSize="small" />
        </IconButton>
      </Box>

      <Menu {...bindMenu(popupState)}>
        <MenuItem onClick={handleResetOrder}>Reiniciar ordenamiento</MenuItem>
        <MenuItem onClick={handleSortAsc}>Ordenar ASC</MenuItem>
        <MenuItem onClick={handleSortDesc}>Ordenar DESC</MenuItem>
        <MenuItem onClick={popupState.close}>Filtrar</MenuItem>
        <MenuItem onClick={handleHide}>Ocultar</MenuItem>
        <MenuItem onClick={handleDisplayHeadMenu}>Mostrar columnas</MenuItem>
      </Menu>
    </>
  );
};

const DataTableHeadItem = ({
  column,
  openTableMenu,
  onChangeColumnVisibility,
  onChangeSortField,
  sortingOptions,
  sx,
}) => {
  const handleChangeVisibility = (visibility) => {
    onChangeColumnVisibility(column.field, visibility);
  };

  const cellRef = useRef();

  return (
    <TableCell key={`column-header-${column.field}`} ref={cellRef} sx={sx}>
      <Box
        className="DataTable-CellContent DataTable-CellContentHeader"
        sx={{
          width: "100%",
          minWidth: "100%",
          maxWidth: "100%",

          "& .DataTable-CellMenu": {
            display: "none",
          },

          ":hover .DataTable-CellMenu": {
            display: "flex",
          },
        }}
      >
        <Typography>{column.headerName}</Typography>
        <DataTableHeaderMenu
          anchorElRef={cellRef}
          column={column}
          onChangeColumnVisibility={handleChangeVisibility}
          onChangeSortField={onChangeSortField}
          sortingOptions={sortingOptions}
          openTableMenu={openTableMenu}
        />
      </Box>
    </TableCell>
  );
};

export default DataTableHeadItem;
