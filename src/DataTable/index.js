import DataTable from "./DataTable";
import DataTableBody from "./DataTableBody";
import DataTableFooter from "./DataTableFooter";
import DataTableHead from "./DataTableHead";

export { DataTableBody, DataTableFooter, DataTableHead };
export default DataTable;
