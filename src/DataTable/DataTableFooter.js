import { Box, TablePagination } from "@mui/material";

const DataTableFooter = ({
  pageNumber = 0,
  pageSize = 0,
  pageSizeOptions = [10, 20, 50],
  totalElements = 0,
  onPageChange,
  onPageSizeChange,
}) => (
  <Box className="DataTable-Footer">
    <TablePagination
      component="div"
      page={pageNumber - 1}
      count={totalElements}
      rowsPerPage={pageSize}
      rowsPerPageOptions={pageSizeOptions}
      onPageChange={(_, page) => onPageChange(page + 1)}
      onRowsPerPageChange={({ target: { value } }) => onPageSizeChange(value)}
    />
  </Box>
);

export default DataTableFooter;
