import {
  Box,
  Button,
  FormControl,
  FormControlLabel,
  FormGroup,
  Popover,
  Switch,
  TextField,
} from "@mui/material";
import { bindPopover } from "material-ui-popup-state";
import { useContext, useState } from "react";
import { DataTableContext } from "./DataTable";

const DataTableMenu = ({ columns, popupState }) => {
  const { isVisible, setVisibility, hideAll, showAll } =
    useContext(DataTableContext);

  const [searchPattern, setSearchPattern] = useState("");

  const filteredColumns = columns.filter((column) => {
    if (typeof searchPattern === "string") {
      const field = column.field.toLowerCase();
      const filter = searchPattern.toLowerCase();

      return field.includes(filter);
    }

    return true;
  });

  return (
    <Popover {...bindPopover(popupState)}>
      <FormControl
        component="fieldset"
        variant="standard"
        sx={{ padding: "14px" }}
      >
        <TextField
          value={searchPattern}
          onChange={(e) => setSearchPattern(e.target.value)}
          label="Columna"
          variant="standard"
        />

        <FormGroup sx={{ padding: "4px" }}>
          {filteredColumns.map(({ field, headerName }) => (
            <FormControlLabel
              key={`data-table-menu-item-${field}`}
              sx={{ display: "flex", justifyContent: "flex-start" }}
              control={
                <Switch
                  size="small"
                  checked={isVisible(field)}
                  onChange={(e) => setVisibility(field, e.target.checked)}
                />
              }
              label={headerName}
            />
          ))}
        </FormGroup>

        <Box
          sx={{ display: "flex", justifyContent: "space-between", gap: "14px" }}
        >
          <Button onClick={showAll} sx={{ padding: "0px" }}>
            Mostrar todas
          </Button>

          <Button onClick={hideAll} sx={{ padding: "0px" }}>
            Ocular todas
          </Button>
        </Box>
      </FormControl>
    </Popover>
  );
};

export default DataTableMenu;
